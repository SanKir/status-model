<?php

namespace App\Service\PeriodReport;

use App\Dto\PeriodReport\PeriodReportInput;
use App\Entity\PeriodReport\Report;
use App\Event\PeriodReport\AbstractReportNotificationEvent;
use App\Event\PeriodReport\ErrorReportedEvent;
use App\Event\PeriodReport\IncomingUploadedEvent;
use App\Event\PeriodReport\OutgoingUploadedEvent;
use App\Event\PeriodReport\PeriodClosedEvent;
use App\Event\PeriodReport\ScanErrorReportedEvent;

class ReportNotificationEventFactory
{
    public function makeEvent(Report $report, PeriodReportInput $reportInput): ?AbstractReportNotificationEvent
    {
        if ($reportInput->isOutgoingErrorReported()) {
            return new ErrorReportedEvent($report);
        } elseif ($reportInput->isScanErrorReported()) {
            return new ScanErrorReportedEvent($report);
        } elseif ($reportInput->isIncomingAdded()) {
            return new IncomingUploadedEvent($report);
        } elseif ($reportInput->isOutgoingAdded()) {
            return new OutgoingUploadedEvent($report);
        } elseif ($reportInput->isPeriodClosed()) {
            return new PeriodClosedEvent($report);
        }

        return null;
    }
}
