<?php

namespace App\Service\PeriodReport;

use App\Dto\PeriodReport\PeriodReportInput;
use App\Entity\PeriodReport\Document;
use App\Entity\PeriodReport\DocumentType;
use App\Entity\PeriodReport\Report;
use Symfony\Component\Workflow\Exception\LogicException;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * @method uploadDocument(Report $report)
 * @method deleteDocument(Report $report)
 * @method sendDocument(Report $report)
 * @method uploadScan(Report $report)
 * @method confirmationScan(Report $report)
 * @method managerSendError(Report $report)
 * @method contractorSendError(Report $report)
 */
class ReportStatusChanger
{
    protected const TRANSITION_UPLOAD_DOCUMENT = 'upload_document';
    protected const TRANSITION_DELETE_DOCUMENT = 'delete_document';
    protected const TRANSITION_SEND_DOCUMENT = 'send_document';
    protected const TRANSITION_UPLOAD_SCAN = 'upload_scan';
    protected const TRANSITION_CONFIRMATION_SCAN = 'confirmation_scan';
    protected const TRANSITION_MANAGER_SEND_ERROR = 'manager_send_error';
    protected const TRANSITION_CONTRACTOR_SEND_ERROR = 'contractor_send_error';

    protected const TRANSITION_METHODS = [
        'uploadDocument' => self::TRANSITION_UPLOAD_DOCUMENT,
        'deleteDocument' => self::TRANSITION_DELETE_DOCUMENT,
        'sendDocument' => self::TRANSITION_SEND_DOCUMENT,
        'uploadScan' => self::TRANSITION_UPLOAD_SCAN,
        'confirmationScan' => self::TRANSITION_CONFIRMATION_SCAN,
        'managerSendError' => self::TRANSITION_MANAGER_SEND_ERROR,
        'contractorSendError' => self::TRANSITION_CONTRACTOR_SEND_ERROR,
    ];

    public function __construct(private WorkflowInterface $reportStateMachine)
    {
    }

    public function __call(string $name, array $arguments)
    {
        if (isset(self::TRANSITION_METHODS[$name]) && isset($arguments[0]) && $arguments[0] instanceof Report) {
            $this->transition($arguments[0], self::TRANSITION_METHODS[$name]);
        }
    }

    protected function transition(Report $report, string $transition)
    {
        if (!$this->reportStateMachine->can($report, $transition)) {
            throw new LogicException(\sprintf('Can\'t apply the \'%s\' transition on report', $transition));
        }

        $this->reportStateMachine->apply($report, $transition);
    }

    public function changeReportStatus(PeriodReportInput $reportInput, Report $report)
    {
        if (\is_bool($ia = $reportInput->isIncomingAdded())) {
            if ($ia) {
                if ($this->checkReportDocsHaveActAndIncoming($report)) {
                    $report->setIncomingAddedAt(new \DateTimeImmutable());
                } else {
                    throw new \InvalidArgumentException('You need to add act and incoming file to Report before change incomingAdded param');
                }
            }

            $report->setIncomingAdded($ia);
        }

        if (\is_bool($oa = $reportInput->isOutgoingAdded())) {
            if ($oa) {
                if ($this->checkReportDocsHaveAct($report)) {
                    $report->setOutgoingErrorReported(false);
                    $report->setOutgoingAddedAt(new \DateTimeImmutable());
                } else {
                    throw new \InvalidArgumentException('You need to add act to Report before change outgoingAdded param');
                }
            }

            $report->setOutgoingAdded($oa);
        }
    }

    private function checkReportDocsHaveAct(Report $report): bool
    {
        /** @var Document $document */
        foreach ($report->getDocuments() as $document) {
            if ($document->getType()?->getName() === DocumentType::MAIN_TYPE_ACT) {
                return true;
            }
        }

        return false;
    }

    private function checkReportDocsHaveActAndIncoming(Report $report): bool
    {
        /** @var Document $document */
        foreach ($report->getDocuments() as $document) {
            if ($document->getType()?->getName() === DocumentType::MAIN_TYPE_ACT && $document->getIncoming() !== null) {
                return true;
            }
        }

        return false;
    }
}
