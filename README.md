# Status model

This code example demonstrates the use of the status model.

## What was used?

Framework: Symfony

Components: Workflow Component, EventDispatcher Component

All information about statuses and transitions is in the `workflow.yaml` file