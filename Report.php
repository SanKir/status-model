<?php declare(strict_types=1);

namespace App\Entity\PeriodReport;

use ApiPlatform\Core\Annotation\{ApiFilter, ApiResource, ApiSubresource};
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\{DateFilter, OrderFilter, SearchFilter};
use App\Controller\PeriodReport\ArchiveController;
use App\Dto\PeriodReport\PeriodReportInput;
use App\Entity\{ContractorRole\AbstractContractorRole, User\Administrator};
use App\Repository\PeriodReport\ReportRepository;
use App\Traits\UuidEntityTrait;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 * @ORM\Table(name="period_report")
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => "is_granted('PERIOD_REPORT', object)"],
        'post' => [
            'input' => PeriodReportInput::class,
            'security_post_denormalize' => "is_granted('PERIOD_REPORT', object)",
        ],
    ],
    itemOperations: [
        'get',
        'put',
        'patch',
        'delete',
        'documents-archive' => [
            'security' => "is_granted('PERIOD_REPORT', object)",
            'method' => 'GET',
            'path' => '/period-reports/{id}/documents-archive',
            'controller' => ArchiveController::class,
            'openapi_context' => [
                'summary' => 'Get archive',
                'description' => 'Download documents in archive',
            ],
        ],
    ],
    shortName: 'PeriodReport',
    input: PeriodReportInput::class,
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'administrator.id' => SearchFilter::STRATEGY_EXACT,
        'contractorRole.id' => SearchFilter::STRATEGY_EXACT,
    ]
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'periodStart' => DateFilter::EXCLUDE_NULL,
        'periodEnd' => DateFilter::EXCLUDE_NULL,
    ]
)]
#[ApiFilter(
    filterClass: OrderFilter::class,
    properties: [
        'periodStart' => 'ASC',
        'periodEnd' => 'ASC',
    ],
)]
#[UniqueEntity(
    fields: ['periodStart', 'periodEnd', 'contractorRole'],
    message: 'This Period Report already exists',
    errorPath: 'contractorRole',
)]
class Report implements \Stringable
{
    use UuidEntityTrait;

    /**
     * @ORM\Column(type="datetimetz_immutable", nullable=false)
     */
    #[Assert\NotBlank]
    private ?\DateTimeInterface $periodStart = null;

    /**
     * @ORM\Column(type="datetimetz_immutable", nullable=false)
     */
    #[Assert\NotBlank]
    private ?\DateTimeInterface $periodEnd = null;

    /**
     * @ORM\ManyToOne(targetEntity=Administrator::class, inversedBy="periodReports")
     */
    private ?Administrator $administrator = null;

    /**
     * @ORM\ManyToOne(targetEntity=AbstractContractorRole::class, inversedBy="periodReports")
     */
    private ?AbstractContractorRole $contractorRole = null;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="report", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    #[ApiSubresource]
    #[Assert\Count(max: 10)]
    #[Assert\Valid]
    private Collection $documents;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $outgoingAdded = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $incomingAdded = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $periodClosed = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $outgoingErrorReported = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $scanErrorReported = false;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     * @Gedmo\Timestampable(on="change", field="outgoingAdded", value=true)
     */
    private ?\DateTimeInterface $outgoingAddedAt = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     * @Gedmo\Timestampable(on="change", field="incomingAdded", value=true)
     */
    private ?\DateTimeInterface $incomingAddedAt = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     * @Gedmo\Timestampable(on="change", field="periodClosed", value=true)
     */
    private ?\DateTimeInterface $periodClosedAt = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $status = null;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function __toString(): string
    {
        $formatter = new \IntlDateFormatter(
            'ru_RU',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE,
            null,
            null,
            'd MMMM yy'
        );

        return \sprintf('%s–%s %s',
            $formatter->format($this->periodStart),
            $formatter->format($this->periodEnd),
            $this->contractorRole?->getContractNumber() ?? 'unknown'
        );
    }

    public function getPeriodStart(): ?\DateTimeInterface
    {
        return $this->periodStart instanceof \DateTimeInterface
            ? \DateTimeImmutable::createFromInterface($this->periodStart)->setTime(0, 0)
            : null;
    }

    public function setPeriodStart(?\DateTimeInterface $periodStart): self
    {
        $this->periodStart = $periodStart;

        return $this;
    }

    public function getPeriodEnd(): ?\DateTimeInterface
    {
        return $this->periodEnd instanceof \DateTimeInterface
            ? \DateTimeImmutable::createFromInterface($this->periodEnd)->setTime(0, 0)
            : null;
    }

    public function setPeriodEnd(?\DateTimeInterface $periodEnd): self
    {
        $this->periodEnd = $periodEnd;

        return $this;
    }

    public function getOutgoingAdded(): ?bool
    {
        return $this->outgoingAdded;
    }

    public function setOutgoingAdded(bool $outgoingAdded): self
    {
        $this->outgoingAdded = $outgoingAdded;

        return $this;
    }

    public function getIncomingAdded(): ?bool
    {
        return $this->incomingAdded;
    }

    public function setIncomingAdded(bool $incomingAdded): self
    {
        $this->incomingAdded = $incomingAdded;

        return $this;
    }

    public function getPeriodClosed(): ?bool
    {
        return $this->periodClosed;
    }

    public function setPeriodClosed(bool $periodClosed): self
    {
        $this->periodClosed = $periodClosed;

        return $this;
    }

    public function isOutgoingErrorReported(): bool
    {
        return $this->outgoingErrorReported;
    }

    public function setOutgoingErrorReported(bool $outgoingErrorReported): self
    {
        $this->outgoingErrorReported = $outgoingErrorReported;

        return $this;
    }

    public function getAdministrator(): ?Administrator
    {
        return $this->administrator;
    }

    public function setAdministrator(?Administrator $administrator): self
    {
        $this->administrator = $administrator;

        return $this;
    }

    public function getContractorRole(): ?AbstractContractorRole
    {
        return $this->contractorRole;
    }

    public function setContractorRole(?AbstractContractorRole $contractorRole): self
    {
        $this->contractorRole = $contractorRole;

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setReport($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document) && $document->getReport() === $this) {
            $document->setReport(null);
        }

        return $this;
    }

    public function setOutgoingAddedAt(?\DateTimeInterface $outgoingAddedAt): self
    {
        $this->outgoingAddedAt = $outgoingAddedAt;

        return $this;
    }

    public function getOutgoingAddedAt(): ?\DateTimeInterface
    {
        return $this->outgoingAddedAt;
    }

    public function setIncomingAddedAt(?\DateTimeInterface $incomingAddedAt): self
    {
        $this->incomingAddedAt = $incomingAddedAt;

        return $this;
    }

    public function getIncomingAddedAt(): ?\DateTimeInterface
    {
        return $this->incomingAddedAt;
    }

    public function setPeriodClosedAt(?\DateTimeInterface $periodClosedAt): self
    {
        $this->periodClosedAt = $periodClosedAt;

        return $this;
    }

    public function getPeriodClosedAt(): ?\DateTimeInterface
    {
        return $this->periodClosedAt;
    }

    public function getPeriodName(): ?string
    {
        if (($periodStart = $this->periodStart) === null) {
            return null;
        }

        $periodName = \IntlDateFormatter::formatObject($periodStart, 'LLLL Y', 'ru');
        $firstLetter = \mb_strtoupper(\mb_substr($periodName, 0, 1));

        return $firstLetter . \mb_substr($periodName, 1);
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getStatusName(): ?string
    {
        return ReportStatus::STATUSES[$this->status] ?? null;
    }

    public function setStatus(string $status): self
    {
        if (!ReportStatus::STATUSES[$status] ?? null) {
            throw new \InvalidArgumentException('Invalid status');
        }

        $this->status = $status;

        return $this;
    }

    public function isScanErrorReported(): bool
    {
        return $this->scanErrorReported;
    }

    public function setScanErrorReported(bool $scanErrorReported): self
    {
        $this->scanErrorReported = $scanErrorReported;

        return $this;
    }
}
