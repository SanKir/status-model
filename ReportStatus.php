<?php declare(strict_types=1);

namespace App\Entity\PeriodReport;

final class ReportStatus
{
    public const STATUS_NEW = 'new';
    public const STATUS_SHAPING = 'shaping';
    public const STATUS_SIGNING = 'signing';
    public const STATUS_PROCESSING = 'processing';
    public const STATUS_SIGNED = 'signed';

    public const STATUSES = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_SHAPING => 'На формировании',
        self::STATUS_SIGNING => 'На подписании',
        self::STATUS_PROCESSING => 'В обработке',
        self::STATUS_SIGNED => 'Подписано',
    ];
}
