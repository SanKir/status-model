<?php

namespace App\Event\PeriodReport;

use App\Entity\PeriodReport\Report;
use Symfony\Contracts\EventDispatcher\Event;

abstract class AbstractReportNotificationEvent extends Event
{
    public function __construct(private Report $report)
    {
    }

    public function getReport(): Report
    {
        return $this->report;
    }
}
