<?php declare(strict_types=1);

namespace App\EventSubscriber\PeriodReport;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\PeriodReport\Document;
use App\Entity\PeriodReport\Report;
use App\Entity\PeriodReport\ReportStatus;
use App\Event\PeriodReport\DocumentPreCreateEvent;
use App\Event\PeriodReport\ErrorReportedEvent;
use App\Event\PeriodReport\IncomingUploadedEvent;
use App\Event\PeriodReport\OutgoingUploadedEvent;
use App\Event\PeriodReport\PeriodClosedEvent;
use App\Event\PeriodReport\ScanErrorReportedEvent;
use App\Service\PeriodReport\ReportStatusChanger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ReportSubscriber implements EventSubscriberInterface
{
    public function __construct(private ReportStatusChanger $statusChanger)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            OutgoingUploadedEvent::class => 'onOutgoingUploaded',
            IncomingUploadedEvent::class => 'onIncomingUploaded',
            ErrorReportedEvent::class => 'onErrorReported',
            ScanErrorReportedEvent::class => 'onScanErrorReported',
            PeriodClosedEvent::class => 'onPeriodClosed',
            DocumentPreCreateEvent::class => 'onShaping',
            KernelEvents::VIEW => ['onDeleteDocument', EventPriorities::PRE_WRITE],
        ];
    }

    public function onShaping(DocumentPreCreateEvent $event)
    {
        $document = $event->getDocument();
        $report = $document->getReport();

        if ($report && !$document->getIncoming() && $report->getStatus() !== ReportStatus::STATUS_SHAPING) {
            $this->statusChanger->uploadDocument($report);
        }
    }

    public function onDeleteDocument(ViewEvent $event)
    {
        $document = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$document instanceof Document || Request::METHOD_DELETE !== $method) {
            return;
        }

        $report = $document->getReport();

        if ($report instanceof Report && $report->getDocuments()->count() === 1) {
            $this->statusChanger->deleteDocument($report);
        }
    }

    public function onOutgoingUploaded(OutgoingUploadedEvent $event)
    {
        $report = $event->getReport();
        $this->statusChanger->sendDocument($report);
    }

    public function onIncomingUploaded(IncomingUploadedEvent $event)
    {
        $report = $event->getReport();
        $this->statusChanger->uploadScan($report);
    }

    public function onErrorReported(ErrorReportedEvent $event)
    {
        $report = $event->getReport();
        $this->statusChanger->contractorSendError($report);
    }

    public function onScanErrorReported(ScanErrorReportedEvent $event)
    {
        $report = $event->getReport();
        $this->statusChanger->managerSendError($report);
    }

    public function onPeriodClosed(PeriodClosedEvent $event)
    {
        $report = $event->getReport();
        $this->statusChanger->confirmationScan($report);
    }
}
