<?php

namespace App\EventSubscriber\PeriodReport;

use App\Entity\PeriodReport\Report;
use App\Entity\User\AbstractUser;
use App\Event\PeriodReport\OutgoingUploadedEvent;
use App\Service\Notifier\MailSender;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Twig\Environment as Twig;

class OutgoingUploadedSubscriber extends AbstractReportNotificationSubscriber
{
    public function __construct(
        private MailSender $mailSender,
        private ParameterBagInterface $bag,
        private Twig $twig,
        private string $frontendDomain
    ) {
        parent::__construct($mailSender, $bag);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            OutgoingUploadedEvent::class => 'onReportNotification',
        ];
    }

    public function getUser(Report $report): ?AbstractUser
    {
        if (($contractor = $report->getContractorRole()?->getContractor()) === null) {
            throw new UnprocessableEntityHttpException('ContractorRole must contain Contractor');
        }

        return $contractor;
    }

    public function getSubject(Report $report): string
    {
        if (($periodName = $report->getPeriodName()) === null) {
            throw new UnprocessableEntityHttpException('Report must contain PeriodName');
        }

        if (($contractNumber = $report->getContractorRole()?->getContractNumber()) === null) {
            throw new UnprocessableEntityHttpException('ContractorRole must contain ContractNumber');
        }

        return \sprintf('Отчетные документы за %s. Договор %s', $periodName, $contractNumber);
    }

    public function getTemplateName(): string
    {
        return 'report/outgoing-uploaded.html.twig';
    }

    public function getContent(Report $report): string
    {
        $reportFrontLink = \sprintf('%s/%s', \rtrim($this->frontendDomain, '/'), 'documents/reports');

        return $this->twig->render($this->getTemplateName(), ['report' => $report, 'reportFrontLink' => $reportFrontLink]);
    }
}
