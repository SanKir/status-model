<?php

namespace App\EventSubscriber\PeriodReport;

use App\Entity\PeriodReport\Report;
use App\Entity\User\AbstractUser;
use App\Event\PeriodReport\PeriodClosedEvent;
use App\Service\Notifier\MailSender;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Twig\Environment as Twig;

class PeriodClosedSubscriber extends AbstractReportNotificationSubscriber
{
    public function __construct(
        private MailSender $mailSender,
        private ParameterBagInterface $bag,
        private Twig $twig
    ) {
        parent::__construct($mailSender, $bag);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            PeriodClosedEvent::class => 'onReportNotification',
        ];
    }

    public function getUser(Report $report): ?AbstractUser
    {
        if (($contractor = $report->getContractorRole()?->getContractor()) === null) {
            throw new UnprocessableEntityHttpException('ContractorRole must contain Contractor');
        }

        return $contractor;
    }

    public function getSubject(Report $report): string
    {
        if (($periodName = $report->getPeriodName()) === null) {
            throw new UnprocessableEntityHttpException('Report must contain PeriodName');
        }

        if (($contractNumber = $report->getContractorRole()?->getContractNumber()) === null) {
            throw new UnprocessableEntityHttpException('ContractorRole must contain ContractNumber');
        }

        return \sprintf('Отчетные документы за %s приняты в работу. Договор %s', $periodName, $contractNumber);
    }

    public function getTemplateName(): string
    {
        return 'report/period-closed.html.twig';
    }

    public function getContent(Report $report): string
    {
        return $this->twig->render($this->getTemplateName(), ['report' => $report]);
    }
}
