<?php

namespace App\EventSubscriber\PeriodReport;

use App\Entity\PeriodReport\Report;
use App\Entity\User\AbstractUser;
use App\Event\PeriodReport\AbstractReportNotificationEvent;
use App\Resources\Mail\SendSingleMail;
use App\Service\Notifier\MailSender;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mime\Address;

abstract class AbstractReportNotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MailSender $mailSender,
        private ParameterBagInterface $bag,
    ) {
    }

    abstract public function getUser(Report $report): ?AbstractUser;

    abstract public function getSubject(Report $report): string;

    abstract public function getContent(Report $report): string;

    abstract public function getTemplateName(): string;

    /**
     * @psalm-suppress ArgumentTypeCoercion, UndefinedMethod
     */
    public function onReportNotification(AbstractReportNotificationEvent $event): void
    {
        $report = $event->getReport();
        $user = $this->getUser($report);

        $entity = (new SendSingleMail())
            ->setUser($user)
            ->setSubject($this->getSubject($report))
            ->setContent($this->getContent($report))
        ;

        $mail = $this->mailSender->makeMailMessage($entity);
        $recipients = $user ?
            $this->mailSender->makeRecipient($entity)
            : new Address($this->bag->get('app.report_email'));

        $this->mailSender->notify($mail, new ArrayCollection([$recipients]));
    }
}
